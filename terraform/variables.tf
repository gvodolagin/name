variable "aws_region" {
  description = "The AWS region things are created in"
  default     = "us-west-2"
}
variable "aws_access_key_id" {
  description = "The AWS access key id"
  default     = "ACCSESS_KEY_ID"
}
variable "aws_secret_access_key" {
  description = "The AWS secret key"
  default     = "SECRET_KEY"
}
variable "az_count" {
  description = "Number of AZs to cover in a given region"
  default     = "2"
}
variable "app_image" {
  description = "Docker image to run in the ECS cluster"
  default     = "registry.gitlab.com/gvodolagin/name:latest"
}
variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 8080
}
variable "app_count" {
  description = "Number of docker containers to run"
  default     = 1
}
variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "1024"
}
variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "2048"
}
variable "rds_identifier" {
default = "postgres"
} 
variable "rds_instance_type" {
default = "db.t2.micro"
}
variable "rds_storage_size" {
default = "5"
}
variable "rds_engine" {
default = "postgres"
}variable "rds_engine_version" {
default = "10"
}
variable "rds_db_name" {
default = "db_name"
}
variable "rds_admin_user" {
default = "postgres"
}
variable "rds_admin_password" {
default = "POSTGRES_PASSWORD"
}
 variable "rds_publicly_accessible" {
default = "true"
}