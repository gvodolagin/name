package com.example.name.repos;

import org.springframework.data.repository.CrudRepository;
import com.example.name.domain.Name;
import java.util.List;

public interface NameRepo extends CrudRepository<Name, Long> {
         List<Name> findFirst5ByOrderByIdDesc();
}

