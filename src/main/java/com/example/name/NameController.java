package com.example.name;

import com.example.name.domain.Name;
import com.example.name.repos.NameRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;
import java.util.Map;

@Controller
public class NameController {
    @Autowired
    private NameRepo nameRepo;

    @GetMapping
    public String main(Map<String, Object> model) {

        Iterable<Name> names = nameRepo.findAll();
        model.put("names", names);
        return "main";
    }

    @PostMapping
    public String add(@RequestParam String text, Map<String, Object> model) {

        Name name = new Name(text);

        nameRepo.save(name);

        List<Name> names = nameRepo.findFirst5ByOrderByIdDesc();
        model.put("text", text);
        model.put("names", names);

        return "main";
    }
}
