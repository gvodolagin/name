output "alb_hostname" {
  value = "${aws_alb.main.dns_name}:8080"
}
output "RDS" {
value = "address: ${aws_db_instance.db.address}"
}