FROM maven:3.5.4-jdk-8-slim
VOLUME /tmp
ADD /target/*.jar /bin/app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","-Dspring.profiles.active=prod","/bin/app.jar"]