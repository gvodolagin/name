# Set up cloudwatch group and log stream and retain logs for 30 days
resource "aws_cloudwatch_log_group" "name_log_group" {
  name              = "/ecs/name-app"
  retention_in_days = 30

  tags {
    Name = "name-log-group"
  }
}
resource "aws_cloudwatch_log_stream" "name_log_stream" {
  name           = "name-log-stream"
  log_group_name = "${aws_cloudwatch_log_group.name_log_group.name}"
}