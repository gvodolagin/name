### ### Local application run ### ###
 ### Build Image and Start

```bash
git clone git@gitlab.com:gvodolagin/name.git
cd name
docker-compose up --build
```
and access to:

* http://localhost:8080

### Stop
```bash
docker-compose stop
```
### Remove app containers and images
```bash
docker-compose rm web
docker-compose rm postgres
docker rmi name_web:latest
docker rmi postgres:10
docker rmi maven:3.5.4-jdk-8-slim
```



### ### Deploy application in aws ### ###

```bash
git clone git@gitlab.com:gvodolagin/name.git
```

### Presetting aws
add role [ecsTaskExecutionRole](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_execution_IAM_role.html)
and copy Role ARN (arn:aws:iam::you_id:role/ecsTaskExecutionRole)
edit the ```/task/app_name.json```  insert your Role ARN   ```"executionRoleArn": "arn:aws:iam::you_id:role/ecsTaskExecutionRole",```

### Deploy ECS RDS 

```bash
cd terraform
```

edit the ```variables.tf``` insert your  ACCSESS_KEY_ID, SECRET_KEY and POSTGRES_PASSWORD

```
variable "aws_access_key_id" {
  description = "The AWS access key id"
  default     = "ACCSESS_KEY_ID"
}
variable "aws_secret_access_key" {
  description = "The AWS secret key"
  default     = "SECRET_KEY"
}
...
variable "rds_admin_password" {
default = "POSTGRES_PASSWORD"
}
```
run
```bash
terraform init
terraform validate
terraform apply
```
after launch you will see
```
Apply complete! Resources: 27 added, 0 changed, 0 destroyed.

Outputs:

RDS = address: postgres.ckekdaeq7ipu.us-west-2.rds.amazonaws.com  <-- your endpoint DB
alb_hostname = name-load-balancer-322257844.us-west-2.elb.amazonaws.com:8080  <-- your hostname application 
```

### ### Deploy application ### ###
run 
```
cd ..
cd /src/main/resources
```

for deploy application edit the ```application-prod.properties``` replace ```spring.datasource.url``` with yours (endpoint DB)
and add in Gitlab yours variables

![ScreenShot](Variables.png)