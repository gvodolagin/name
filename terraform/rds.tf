resource "aws_db_instance" "db" {
  engine = "${var.rds_engine}"
  engine_version = "${var.rds_engine_version}"
  identifier = "${var.rds_identifier}"
  instance_class = "${var.rds_instance_type}"
  allocated_storage = "${var.rds_storage_size}"
  name = "${var.rds_db_name}"username = "${var.rds_admin_user}"
  password = "${var.rds_admin_password}"
  publicly_accessible = "${var.rds_publicly_accessible}"
  vpc_security_group_ids = ["${aws_security_group.rds_security_group.id}"]
  skip_final_snapshot = true
  tags {
    Name = "IAC Database in ${var.aws_region}"
  }
}